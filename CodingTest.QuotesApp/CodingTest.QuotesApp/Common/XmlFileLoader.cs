﻿using CodingTest.QuotesApp.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CodingTest.QuotesApp.Common
{
    public class XmlFileLoader : IQuotesFileLoader
    {
        #region Async Methods
        /// <summary>
        /// This method is responsible for upload the xml file asynchronous
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        async Task<List<Quote>> IQuotesFileLoader.Read(string file)
        {
            string content = null;

            using (var stream = new StreamReader(file, Encoding.UTF8))
            {
                content = await stream.ReadToEndAsync();
            }

            var doc = XDocument.Parse(content);

            return
                doc.Root.Descendants().Select(el => new Quote()
                {
                    Source = new Source() { Name = el.Attribute("source").Value, Description = Path.GetFileName(file) },
                    Date = Convert.ToDateTime(el.Attribute("date").Value),
                    Open = Convert.ToDecimal(el.Attribute("open").Value),
                    High = Convert.ToDecimal(el.Attribute("high").Value),
                    Low = Convert.ToDecimal(el.Attribute("low").Value),
                    Close = Convert.ToDecimal(el.Attribute("close").Value),
                    Volume = Convert.ToInt64(el.Attribute("volume").Value)
                }).ToList();
        }

        #endregion
    }
}
