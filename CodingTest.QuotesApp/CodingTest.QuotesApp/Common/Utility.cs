﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CodingTest.FileLoader.Common
{
    public static class Utility
    {
        #region Static Methods
        /// <summary>
        /// This method is responsible for return a list of files that should be uploaded
        /// </summary>
        /// <param name="targetDirectory"></param>
        /// <param name="getOnlyCsv"></param>
        /// <param name="getOnlyXML"></param>
        /// <returns></returns>
        public static List<string> GetFiles(string targetDirectory, bool getOnlyCsv, bool getOnlyXML)
        {
            if (Directory.Exists(targetDirectory))
            {
                var searchPattern = new Regex(@"$(?<=\.(xml|csv))", RegexOptions.IgnoreCase);

                if (getOnlyCsv && !getOnlyXML)
                {
                    // ONLY CVS
                    searchPattern = new Regex(@"$(?<=\.(csv))", RegexOptions.IgnoreCase);
                }
                else if (!getOnlyCsv && getOnlyXML)
                {
                    // ONLY XML
                    searchPattern = new Regex(@"$(?<=\.(xml))", RegexOptions.IgnoreCase);
                }

                return Directory.EnumerateFiles(targetDirectory).Where(f => searchPattern.IsMatch(f)).ToList();
            }
            return new List<string>();
        }
        #endregion

    }
}
