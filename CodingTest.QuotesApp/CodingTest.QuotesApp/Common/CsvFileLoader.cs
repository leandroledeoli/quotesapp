﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodingTest.QuotesApp.Entity;
using System.IO;

namespace CodingTest.QuotesApp.Common
{
    public class CsvFileLoader : IQuotesFileLoader
    {
        #region Async Methods
        /// <summary>
        /// This method is responsible for Read the files asynchronous
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        async Task<List<Quote>> IQuotesFileLoader.Read(string file)
        {
            string content = null;
            List<Quote> quotesCollection = new List<Quote>();

            using (var stream = new StreamReader(file, Encoding.UTF8))
            {
                while (!stream.EndOfStream)
                {
                    content = await stream.ReadLineAsync();
                    //THE UPLOADED CSV CONTAINS ',' INSTEAD OF ';' That's why I'm uploading in this format.
                    var listElements = content.Split(',');

                    if (listElements.Length < 7)
                        return default(List<Quote>);

                    quotesCollection.Add(new Quote(listElements, Path.GetFileName(file)));
                }
            }
            return quotesCollection;
        }
        #endregion
    }
}
