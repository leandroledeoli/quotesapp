﻿using CodingTest.FileLoader.Common;
using CodingTest.QuotesApp;
using CodingTest.QuotesApp.Common;
using CodingTest.QuotesApp.DataAccess;
using CodingTest.QuotesApp.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodingTest.QuotesApp
{
    public partial class Form1 : Form
    {
        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            StartControls();

        }
        #endregion

        #region Attribute
        private System.Timers.Timer _timer;
        private List<string> _uploadedFiles = new List<string>();
        #endregion

        #region Private Methods
        /// <summary>
        /// This method is responsible for get the data in the files and insert in the tables
        /// </summary>
        /// <param name="file"></param>
        private async void InsertXmlData(string file)
        {
            IQuotesFileLoader loadXML = new XmlFileLoader();
            IRepository<Quote> repository = new QuoteRepository();
            var xmlCollection = await loadXML.Read(file);
            repository.Insert(xmlCollection);
            GetPopulatedFiles(file);
        }

        /// <summary>
        /// This method is responsible for get the data in the files and insert in the tables
        /// </summary>
        /// <param name="file"></param>
        private async void InserCsvData(string file)
        {
            IQuotesFileLoader loadCSV = new CsvFileLoader();
            IRepository<Quote> repository = new QuoteRepository();
            var csvCollection = await loadCSV.Read(file);
            repository.Insert(csvCollection);
            GetPopulatedFiles(file);

        }

        /// <summary>
        /// This method is responsible for starting the controls
        /// </summary>
        private void StartControls()
        {
            btnStop.Enabled = false;
            btnStart.Enabled = string.IsNullOrEmpty(txtPath.Text) ? false : true;
        }

        /// <summary>
        /// This method is responsible for start listening for x seconds
        /// </summary>
        private void StartListening()
        {
            var timerMilliseconds = timerMonitoring.Value * 1000;

            _timer = new System.Timers.Timer((int)timerMilliseconds);
            _timer.Start();
            _timer.Elapsed += Timer_Elapsed;
        }

        /// <summary>
        /// This method is responsible for populate in the UI the uploaded files
        /// </summary>
        private void GetPopulatedFiles(string file)
        {
            if (!_uploadedFiles.Contains(file))
                _uploadedFiles.Add(file);

            Invoke(new Action(() =>
            {
                txtUploadedFiles.Text = String.Join(Environment.NewLine, _uploadedFiles);
            }));
        }
        #endregion

        #region Events
        /// <summary>
        /// This method is responsible for open a Dialog browser to select a monitoring folder
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBrowser_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtPath.Text = folderBrowserDialog1.SelectedPath;
                btnStart.Enabled = true;
            }
        }
        /// <summary>
        /// This method is responsible for stopping to monitoring the selected folder
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStop_Click(object sender, EventArgs e)
        {
            if (_timer != null)
            {
                _timer.Stop();
                _timer.Elapsed -= Timer_Elapsed;
                btnStart.Enabled = true;
                btnStop.Enabled = false;
            }
        }
        /// <summary>
        /// This method is responsible for starting to listen the folder for new files
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStart_Click(object sender, EventArgs e)
        {
            StartListening();
            btnStart.Enabled = false;
            btnStop.Enabled = true;
        }

        /// <summary>
        /// This method is responsible for upload the files into database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            var listOfFiles = Utility.GetFiles(txtPath.Text, chkOnlyCSV.Checked, chkOnlyXML.Checked);
            foreach (var itemFile in listOfFiles)
            {
                if (Path.GetExtension(itemFile).Equals(".csv"))
                {
                    InserCsvData(itemFile);
                }
                else
                {
                    InsertXmlData(itemFile);
                }
            }
        }
        #endregion
    }
}
