﻿using CodingTest.QuotesApp.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingTest.QuotesApp.Entity
{
    [Table("Source")]
    public class Source
    {
        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public Source()
        {
            Quote = new HashSet<Quote>();
        }
        #endregion

        #region Properties

        /// <summary>
        /// Represent the field SourceID in the Table dbo.[Source]
        /// </summary>
        [Key]
        public int SourceID { get; set; }

        /// <summary>
        /// Represent the field Name in the Table dbo.[Source]
        /// </summary>
        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// Represent the field Description in the Table dbo.[Source]
        /// </summary>
        [StringLength(300)]
        public string Description { get; set; }

        /// <summary>
        /// Represent a collection of quote whichh belong to its code
        /// </summary>
        public virtual ICollection<Quote> Quote { get; set; }
        #endregion

    }
}
