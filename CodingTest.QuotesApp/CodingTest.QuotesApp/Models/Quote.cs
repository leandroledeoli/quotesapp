﻿using CodingTest.QuotesApp.DataAccess;
using CodingTest.QuotesApp.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingTest.QuotesApp.Entity
{
    [Table("Quote")]
    public class Quote
    {
        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public Quote()
        {

        }

        /// <summary>
        /// Constructor with all properties
        /// </summary>
        /// <param name="sourceID"></param>
        /// <param name="date"></param>
        /// <param name="open"></param>
        /// <param name="high"></param>
        /// <param name="low"></param>
        /// <param name="close"></param>
        /// <param name="volume"></param>
        public Quote(int sourceID, DateTime date, decimal open, decimal high, decimal low, decimal close, long volume)
        {
            SourceID = sourceID;
            Date = date;
            Open = open;
            High = high;
            Low = low;
            Close = close;
            Volume = volume;
        }

        /// <summary>
        /// Constructor available for CSV files
        /// </summary>
        /// <param name="csvArray"></param>
        /// <param name="sourceDescription"></param>
        public Quote(string[] csvArray, string sourceDescription)
        {
            /*
             * 0 - Source Name
             * 1 - Date
             * 2 - Open
             * 3 - high
             * 4 - low
             * 5 - close
             * 6 - volume */
            if (csvArray.Length < 7)
                return;

            DateTime dateParse;
            Date = DateTime.TryParse(csvArray[1], out dateParse) ? dateParse : DateTime.Today;

            Decimal openParse;
            Decimal.TryParse(csvArray[2], out openParse);
            Open = openParse;

            Decimal highParse;
            Decimal.TryParse(csvArray[3], out highParse);
            High = highParse;

            Decimal lowParse;
            Decimal.TryParse(csvArray[4], out lowParse);
            Low = lowParse;

            Decimal closeParse;
            Decimal.TryParse(csvArray[5], out closeParse);
            Close = closeParse;

            long volumeParse;
            long.TryParse(csvArray[6], out volumeParse);
            Volume = volumeParse;

            Source = new Source()
            {
                Name = csvArray[0],
                Description = sourceDescription
            };
        }

        #endregion

        #region Properties
        /// <summary>
        /// Represent the field QuoteID in the Table dbo.[Quote]
        /// </summary>
        [Key]
        public int QuoteID { get; set; }

        /// <summary>
        /// Represent the field SourceID in the Table dbo.[Quote]
        /// </summary>
        public int SourceID { get; set; }

        /// <summary>
        /// Represent the field Date in the Table dbo.[Quote]
        /// </summary>
        public DateTime? Date { get; set; } = DateTime.Now;

        /// <summary>
        /// Represent the field Open in the Table dbo.[Quote]
        /// </summary>
        public decimal Open { get; set; }

        /// <summary>
        /// Represent the field High in the Table dbo.[Quote]
        /// </summary>
        public decimal High { get; set; }

        /// <summary>
        /// Represent the field Low in the Table dbo.[Quote]
        /// </summary>
        public decimal Low { get; set; }

        /// <summary>
        /// Represent the field Close in the Table dbo.[Quote]
        /// </summary>
        public decimal Close { get; set; }

        /// <summary>
        /// Represent the field Volume in the Table dbo.[Quote]
        /// </summary>
        public long? Volume { get; set; }

        /// <summary>
        /// Represent the field Volume in the Table dbo.[Quote]
        /// </summary>
        public virtual Source Source { get; set; }
    }
    #endregion
}
