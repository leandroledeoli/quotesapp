namespace CodingTest.FileLoader.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Quote")]
    public partial class Quote
    {
        public int QuoteID { get; set; }

        public int SourceID { get; set; }

        public DateTime? Date { get; set; }

        public decimal Open { get; set; }

        public decimal High { get; set; }

        public decimal Low { get; set; }

        public decimal Close { get; set; }

        public long? Volume { get; set; }

        public virtual Source Source { get; set; }
    }
}
