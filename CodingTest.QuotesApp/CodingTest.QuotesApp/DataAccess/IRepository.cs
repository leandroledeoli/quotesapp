﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingTest.QuotesApp.DataAccess
{
    public interface IRepository<T> where T : class
    {
        #region MyRegion

        /// <summary>
        /// This method is responsible for returning all the data in a specific table
        /// </summary>
        /// <returns></returns>
        Task<List<T>> GetAll();

        /// <summary>
        /// This methosd is responsible for add an element in DataBase from diferent sources (XML or CSV)
        /// </summary>
        /// <param name="entity"></param>
        void Insert(List<T> entity);

        /// <summary>
        /// This method is responsible for updating a specific data  
        /// NOT AVAILABLE
        /// </summary>
        /// <param name="entity"></param>
        void Update(T entity);

        /// <summary>
        /// This method is responsible for deleting a specific data
        /// NOT AVAILABLE
        /// </summary>
        /// <param name="entity"></param>
        void Delete(T entity);
        #endregion
    }
}
