﻿using CodingTest.QuotesApp.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingTest.QuotesApp.DataAccess
{
    public class QuoteRepository : IRepository<Quote>
    {
        #region Async Methods

        /// <summary>
        /// Delete an item - NOT AVAILABLE
        /// </summary>
        /// <param name="entity"></param>
        async void IRepository<Quote>.Delete(Quote entity)
        {
            using (var context = new QuotesDataContext())
            {
                context.Quote.Remove(entity);
                await context.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Search data - NOT AVAILABLE
        /// </summary>
        /// <returns></returns>
        async Task<List<Quote>> IRepository<Quote>.GetAll()
        {
            using (var context = new QuotesDataContext())
            {
                var result = await context.Quote.SqlQuery("SELECT * FROM dbo.Quote").ToListAsync();
                return result;
            }

        }

        /// <summary>
        /// Insert an element from file 
        /// </summary>
        /// <param name="entityCollection"></param>
        async void IRepository<Quote>.Insert(List<Quote> entityCollection)
        {
            using (var context = new QuotesDataContext())
            {
                if (entityCollection != null)
                {
                    foreach (var entityElement in entityCollection)
                    {
                        await context.Database.ExecuteSqlCommandAsync("[INSERT_QUOTE_FILE] @SOURCE, @DATE, @OPEN, @HIGH, @LOW, @CLOSE, @VOLUME, @DESCRIPTION",
                                                          new SqlParameter("@SOURCE", entityElement.Source.Name),
                                                          new SqlParameter("@DATE", entityElement.Date),
                                                          new SqlParameter("@OPEN", entityElement.Open),
                                                          new SqlParameter("@HIGH", entityElement.High),
                                                          new SqlParameter("@LOW", entityElement.Low),
                                                          new SqlParameter("@CLOSE", entityElement.Close),
                                                          new SqlParameter("@VOLUME", entityElement.Volume),
                                                          new SqlParameter("@DESCRIPTION", entityElement.Source.Description));
                        await context.SaveChangesAsync();
                    }
                }
            }
        }

        /// <summary>
        /// Update an element from file - NOT AVAILABLE
        /// </summary>
        /// <param name="entity"></param>
        async void IRepository<Quote>.Update(Quote entity)
        {
            using (var context = new QuotesDataContext())
            {
                context.Quote.Attach(entity);

                var entry = context.Entry(entity);
                entry.State = EntityState.Modified;

                entry.Property(e => e.QuoteID).IsModified = false;
                entry.Property(e => e.SourceID).IsModified = false;
                await context.SaveChangesAsync();
            }
        }
        #endregion
    }
}

