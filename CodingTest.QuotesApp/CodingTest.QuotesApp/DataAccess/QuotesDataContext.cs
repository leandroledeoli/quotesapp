namespace CodingTest.QuotesApp.DataAccess
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using CodingTest.QuotesApp.Entity;

    public partial class QuotesDataContext : DbContext
    {
        #region Constructor
        public QuotesDataContext()
            : base("name=QuoteData")
        {
        }
        #endregion

        #region Tables
        /// <summary>
        /// Represent the dbo.[Quote] in SQL Server
        /// </summary>
        public virtual DbSet<Quote> Quote { get; set; }

        /// <summary>
        /// Represent the dbo.[Source] in SQL Server
        /// </summary>
        public virtual DbSet<Source> Source { get; set; }
        #endregion

        #region Method
        /// <summary>
        /// Responsible for defining which column belongs to its class
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Source>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Source>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Source>()
                .HasMany(e => e.Quote)
                .WithRequired(e => e.Source)
                .WillCascadeOnDelete(false);
        }
        #endregion
    }
}
