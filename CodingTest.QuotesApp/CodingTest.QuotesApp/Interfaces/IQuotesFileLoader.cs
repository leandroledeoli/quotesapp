﻿using CodingTest.QuotesApp.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingTest.QuotesApp
{
    public interface IQuotesFileLoader
    {
        /// <summary>
        /// Responsible for reading a file
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        Task<List<Quote>> Read(string file);
    }
}
