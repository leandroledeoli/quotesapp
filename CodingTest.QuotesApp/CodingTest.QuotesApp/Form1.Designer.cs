﻿namespace CodingTest.QuotesApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnBrowser = new System.Windows.Forms.Button();
            this.timerMonitoring = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.chkOnlyXML = new System.Windows.Forms.CheckBox();
            this.chkOnlyCSV = new System.Windows.Forms.CheckBox();
            this.txtUploadedFiles = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.timerMonitoring)).BeginInit();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(98, 135);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(126, 49);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "START";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(284, 135);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(126, 49);
            this.btnStop.TabIndex = 1;
            this.btnStop.Text = "STOP";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // txtPath
            // 
            this.txtPath.Location = new System.Drawing.Point(98, 19);
            this.txtPath.Name = "txtPath";
            this.txtPath.ReadOnly = true;
            this.txtPath.Size = new System.Drawing.Size(345, 20);
            this.txtPath.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Select a Path:";
            // 
            // btnBrowser
            // 
            this.btnBrowser.Location = new System.Drawing.Point(467, 19);
            this.btnBrowser.Name = "btnBrowser";
            this.btnBrowser.Size = new System.Drawing.Size(88, 23);
            this.btnBrowser.TabIndex = 4;
            this.btnBrowser.Text = "Browser";
            this.btnBrowser.UseVisualStyleBackColor = true;
            this.btnBrowser.Click += new System.EventHandler(this.btnBrowser_Click);
            // 
            // timerMonitoring
            // 
            this.timerMonitoring.Location = new System.Drawing.Point(117, 63);
            this.timerMonitoring.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.timerMonitoring.Name = "timerMonitoring";
            this.timerMonitoring.Size = new System.Drawing.Size(61, 20);
            this.timerMonitoring.TabIndex = 5;
            this.timerMonitoring.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Time Monitoring";
            // 
            // chkOnlyXML
            // 
            this.chkOnlyXML.AutoSize = true;
            this.chkOnlyXML.Checked = true;
            this.chkOnlyXML.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkOnlyXML.Location = new System.Drawing.Point(254, 65);
            this.chkOnlyXML.Name = "chkOnlyXML";
            this.chkOnlyXML.Size = new System.Drawing.Size(73, 17);
            this.chkOnlyXML.TabIndex = 7;
            this.chkOnlyXML.Text = "Filter XML";
            this.chkOnlyXML.UseVisualStyleBackColor = true;
            // 
            // chkOnlyCSV
            // 
            this.chkOnlyCSV.AutoSize = true;
            this.chkOnlyCSV.Checked = true;
            this.chkOnlyCSV.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkOnlyCSV.Location = new System.Drawing.Point(254, 89);
            this.chkOnlyCSV.Name = "chkOnlyCSV";
            this.chkOnlyCSV.Size = new System.Drawing.Size(72, 17);
            this.chkOnlyCSV.TabIndex = 8;
            this.chkOnlyCSV.Text = "Filter CSV";
            this.chkOnlyCSV.UseVisualStyleBackColor = true;
            // 
            // txtUploadedFiles
            // 
            this.txtUploadedFiles.Location = new System.Drawing.Point(18, 221);
            this.txtUploadedFiles.Multiline = true;
            this.txtUploadedFiles.Name = "txtUploadedFiles";
            this.txtUploadedFiles.ReadOnly = true;
            this.txtUploadedFiles.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.txtUploadedFiles.Size = new System.Drawing.Size(425, 110);
            this.txtUploadedFiles.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 202);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Uploaded Files:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(565, 364);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtUploadedFiles);
            this.Controls.Add(this.chkOnlyCSV);
            this.Controls.Add(this.chkOnlyXML);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.timerMonitoring);
            this.Controls.Add(this.btnBrowser);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPath);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.timerMonitoring)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnBrowser;
        private System.Windows.Forms.NumericUpDown timerMonitoring;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.CheckBox chkOnlyXML;
        private System.Windows.Forms.CheckBox chkOnlyCSV;
        private System.Windows.Forms.TextBox txtUploadedFiles;
        private System.Windows.Forms.Label label3;
    }
}

