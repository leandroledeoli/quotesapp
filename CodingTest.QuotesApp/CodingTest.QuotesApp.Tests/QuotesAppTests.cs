﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CodingTest.FileLoader.Common;
using System.Collections.Generic;
using CodingTest.QuotesApp.DataAccess;
using CodingTest.QuotesApp.Entity;
using Moq;
using System.Data.Entity;
using System.Threading.Tasks;

namespace CodingTest.QuotesApp.Tests
{
    [TestClass]
    public class QuotesAppTests
    {
        [TestMethod]
        public void TestUtilityUnreachableDirectory()
        {
            var invalidDirectory = @"J:\Files";
            var result = Utility.GetFiles(invalidDirectory, false, false);
            Assert.IsTrue(result.Count == 0);
        }

        [TestMethod]
        public async Task TestSaveQuoteInvalidDataTypes()
        {
            var mock = new Mock<IRepository<Quote>>();
            mock.Setup(x => x.Insert(It.IsAny<List<Quote>>())).Verifiable();
        }
    }
}
