CREATE PROCEDURE [dbo].[INSERT_QUOTE_FILE]          
 @SOURCE VARCHAR(50),          
 @DATE DATETIME,          
 @OPEN DECIMAL,          
 @HIGH DECIMAL,          
 @LOW DECIMAL,          
 @CLOSE DECIMAL,          
 @VOLUME INT,  
 @DESCRIPTION VARCHAR(300)  
   
AS          
BEGIN          
/*          
 OPERATION:           
 1 - UPDATE          
 2 - DELETE          
*/          
           
 DECLARE @HASSOURCE INT          
 DECLARE @HASQUOTE INT        
 DECLARE @LASTSOURCEID INT          
 DECLARE @SOURCEID INT          
          
 SELECT           
 @SOURCEID = SourceID            
 FROM SOURCE (NOLOCK) WHERE Name = @SOURCE          
           
 -- CHECK IF THERE IS ALREDY A SOURCE IN DB          
 IF @SOURCEID > 0           
 BEGIN      
     
 -- CHECK TO NOT ADD THE SAME REGISTRY    
 SELECT     
 @HASQUOTE = COUNT(0)    
 FROM dbo.[Quote] WHERE SOURCEID = @SOURCEID AND     
 [Date] = @DATE AND [Open] = @OPEN AND [High] = @HIGH AND [Low] = @LOW AND [Close] = @CLOSE AND [Volume] = @VOLUME    
    
    
 IF @HASQUOTE = 0    
 BEGIN    
   INSERT INTO dbo.[Quote] ([SourceID], [Date], [Open], [High], [Low], [Close], [Volume])          
     SELECT           
   @SOURCEID,          
   @DATE,          
   @OPEN,          
   @HIGH,          
   @LOW,          
   @CLOSE,          
   @VOLUME          
 END    
 END          
 ELSE          
 BEGIN          
  -- ADD THE NEW SOURCE          
  INSERT INTO dbo.[SOURCE] ([Name],[Description]) VALUES (@SOURCE, @DESCRIPTION)          
  SELECT @LASTSOURCEID = SCOPE_IDENTITY()          
          
  INSERT INTO dbo.[Quote] ([SourceID], [Date], [Open], [High], [Low], [Close], [Volume])          
   SELECT           
   @LASTSOURCEID,          
   @DATE,          
   @OPEN,          
   @HIGH,          
   @LOW,          
   @CLOSE,          
   @VOLUME          
 END          
END